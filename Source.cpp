#include <iostream>
#include <cstdio>
#include <ctime>
using namespace std;

void selectionSort(int *a, int n);
void Heapify(int *a, int n, int i);
void buildHeap(int *a, int n);
void heapSort(int *a, int n);
void radixSort(int *a, int n);
void print(int *a, int n);
void Random(int *a, int n);

int main()
{
	int *arr;
	int n;

	cout << "Input n = ";
	cin >> n;

	arr = new int[n];
	Random(arr, n);
	clock_t s1, s2, s3;
	long double duration1, duration2, duration3;
	s1 = clock();
	radixSort(arr, n);
	duration1 = (clock() - s1) / (long double)CLOCKS_PER_SEC;
	cout << "Duration Radixsort: " << duration1 << '\n';
	s2 = clock();
	selectionSort(arr, n);
	duration2 = (clock() - s2) / (long double)CLOCKS_PER_SEC;
	cout << "Duration selectionsort : " << duration2 << '\n';
	s3 = clock();
	heapSort(arr, n);
	duration3 = (clock() - s3) / (long double)CLOCKS_PER_SEC;
	cout << "Duration heapsort : " << duration3 << '\n';
	delete[]arr;
	arr = NULL;
	system("pause");
	return 0;
}

void selectionSort(int *a, int n)
{
	int min, tmp;
	for (int i = 0; i < n; i++)
	{
		min = i;
		for (int j = i + 1; j < n; j++)
			if (a[j] < a[min])
				min = j;
		if (a[min] < a[i])
		{
			tmp = a[i];
			a[i] = a[min];
			a[min] = tmp;
		}
	}
}

void Heapify(int *a, int n, int i)
{
	int tmp = a[i];
	while (i < n / 2)
	{
		int child = 2 * i + 1;
		if (child < n - 1)
			if (a[child] < a[child + 1])
				child++;
		if (tmp >= a[child])
			break;
		a[i] = a[child];
		i = child;
	}
	a[i] = tmp;
}


void buildHeap(int *a, int n)
{
	for (int i = n / 2 - 1; i >= 0; i--)
		Heapify(a, n, i);
}

void heapSort(int *a, int n)
{
	int tmp = 0;
	buildHeap(a, n);
	for (int i = n - 1; i >= 0; i--)
	{
		tmp = a[0];
		a[0] = a[i];
		a[i] = tmp;
		Heapify(a, i, 0);
	}
}

void radixSort(int *a, int n)

{
	int i;
	int max = a[0];
	for (i = 1; i < n; i++)
	{
		if (a[i] > max)
			max = a[i];
	}

	int exp = 1;
	int *tmpBuffer = new int[n];
	while (max / exp > 0)
	{
		int bucket[10] = { 0 };
		for (i = 0; i < n; i++)
			bucket[a[i] / exp % 10]++;

		for (i = 1; i < 10; i++)
			bucket[i] += bucket[i - 1];

		for (i = n - 1; i >= 0; i--)
			tmpBuffer[--bucket[a[i] / exp % 10]] = a[i];
		for (i = 0; i < n; i++)
			a[i] = tmpBuffer[i];

		exp *= 10;

	}
	delete[]tmpBuffer;
}

void print(int *a, int n)
{
	for (int i = 0; i < n; i++)
		cout << a[i] << endl;
}

void Random(int *a, int n)
{
	for (int i = 0; i < n; i++)
		a[i] = rand();
}